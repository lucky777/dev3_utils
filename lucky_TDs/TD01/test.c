#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "mathesi.h"

int main() {
    printf("%d\n", isPrime(45632));
    printf("%d\n", isPrime(11));
    printPrimes(3, 346);
    printPrimeFactor(126, false);
    printPrimeFactor(126, true);
    printf("gcd %d, %d = %d\n", 7, 59, gcd(7, 59));
    printf("gcd %d, %d = %d\n", 429, 130, gcd(429, 130));
    printf("gcd %d, %d = %d\n", 130, 429, gcd(130, 429));
    printGcd();
}