// DEV3 Labo 2

#include "td2.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
    const int test[] = {14, 57, 94, 563, 15, 123, 457, 4511, 123, 54175, 12};
    arrayIntPrint(test, 7);
    arrayIntPrint(test, 3);
    int test2[] = {25, 98, 1, 75, 24, 56, 458, 547, 54175, 456, 1, 25};
    arrayIntSort(test2, 7, true);
    arrayIntSort(test2, 7, false);
    arrayIntSort(test2, 12, false);
    arrayIntSort2(test2, 12);
    arrayIntSortGeneric(test2, 12, compare_ints_descending);
    arrayIntSortGeneric(test2, 12, compare_ints_mod3);
    arrayIntSortGeneric(test2, 12, compare_ints_ascending);
}

// Exercice 2.1
void arrayIntPrint(const int data[], unsigned nbElem) {
    for (int i=0; i<nbElem; i++) {
        printf("%d", data[i]);
        if (i<nbElem-1) {
            printf(" ");
        }
    }
    printf("\n");
}

// Exercice 2.2
// Bubble sorting (tri bulle)
void arrayIntSort(int data[], unsigned nbElem, bool ascending) {
    for (int i=0; i<nbElem-1; i++) {
        for (int j=0; j<nbElem-i-1; j++) {
            if (ascending && data[j]>data[j+1] || !ascending && data[j]<data[j+1]) {
                int tmp = data[j];
                data[j] = data[j+1];
                data[j+1] = tmp;
            }
        }
    }
    arrayIntPrint(data, nbElem);
}

// Exercice 2.3
void arrayIntSort2(int data[], size_t size) {
    //size_t size = sizeof(data) / sizeof(int);
    qsort(data, size, sizeof(int), compare_ints_ascending);
    arrayIntPrint(data, (unsigned)size);
}

int compare_ints_ascending(const void* a, const void* b)
{
    int arg1 = *(const int*)a;
    int arg2 = *(const int*)b;
 
    if (arg1 < arg2) return -1;
    if (arg1 > arg2) return 1;
    return 0;
}

int compare_ints_descending(const void* a, const void* b)
{
    int arg1 = *(const int*)a;
    int arg2 = *(const int*)b;
 
    if (arg1 > arg2) return -1;
    if (arg1 < arg2) return 1;
    return 0;
}

int compare_ints_mod3(const void* a, const void* b)
{
    int arg1 = *(const int*)a;
    int arg2 = *(const int*)b;
 
    if (arg1%3 < arg2%3) return -1;
    if (arg1%3 > arg2%3) return 1;
    return 0;
}

// Exercice 2.4
void arrayIntSortGeneric(int data [], unsigned nbElem, int (*comp)(const void *, const void *)) {
    qsort(data, (size_t)nbElem, sizeof(int), comp);
    arrayIntPrint(data, nbElem);
}