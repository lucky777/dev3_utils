#include "slcircularlist_utility.h"
#include <stdlib.h>
#include <stdio.h>

size_t sizeSLCL(struct SLCircularList* pSLCL) {
    size_t cpt = 1;
    if (pSLCL->entry == NULL) {
        return 0;
    }
    struct SLNode* tmp = pSLCL->entry;
    while(tmp->next != pSLCL->entry) {
        tmp = tmp->next;
        cpt++;
    }
    return cpt;
}

struct SLNode* previousSLCL(const struct SLCircularList* pSLCL, const struct SLNode* pSLN) {
    struct SLNode* previous = pSLCL->entry;

    while(previous->next != pSLN) {
        previous = previous->next;
    }

    return previous;
}