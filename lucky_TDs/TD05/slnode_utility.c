#include "slnode_utility.h"
#include <stdlib.h>
#include <stdio.h>

struct SLNode* forwardSLN(struct SLNode* pSLN, size_t distance) {
    struct SLNode* tmp = psLN;
    for(int i=0; i<distance; i++) {
        tmp = tmp->next;
        if (tmp == NULL) {
            break;
        }
    }
    return tmp;
}

// NODE  |  NODE  |  NODE  |  NODE  |  NODE  |  NODE  |  NODE  |  NODE
//  TMP

int main() {
    printf("Hello world!\n");
    return 0;
}