#include "slcircularlist.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

struct SLCircularList* newSLCL(void) {
    struct SLCircularList* tmp;
    tmp = (struct SLCircularList*)malloc(sizeof(struct SLCircularList));
    if (tmp==NULL) {
        errno = ESLLMEMORYFAIL;
        return NULL;
    }
    tmp->entry = NULL;
    return tmp;
}

void deleteSLCL(struct SLCircularList** adpSLCL) {

    struct SLNode* tmp;

    while((*adpSLCL)->entry != NULL) {
        tmp = (*adpSLCL)->entry;
        while(tmp->next->next != (*adpSLCL)->entry) {
            tmp = tmp->next;
        }
        deleteSLN(&tmp->next);
        if(tmp != (*adpSLCL)->entry) { //Si on supprime le dernier élément on ne devra pas changer le next
            setNextSLN(tmp, (*adpSLCL)->entry);
        }
    }

    free(*adpSLCL);
    *adpSLCL = NULL;
}

// Node

void clearSLCL(struct SLCircularList* pSLCL) {
    struct SLNode* tmp;

    while(pSLCL->entry != NULL) {
        tmp = pSLCL->entry;
        while(tmp->next->next != pSLCL->entry) {
            tmp = tmp->next;
        }
        deleteSLN(&tmp->next);
        if(tmp != pSLCL->entry) { //Si on supprime le dernier élément on ne devra pas changer le next
            setNextSLN(tmp, pSLCL->entry);
        }
    }

    pSLCL->entry = NULL;
}

struct SLNode* entrySLCL(const struct SLCircularList* pSLCL) {
    return pSLCL->entry;
}

bool emptySLCL(const struct SLCircularList* pSLCL) {
    return (pSLCL->entry == NULL);
}

struct SLNode* pushSLCL(struct SLCircularList* pSLCL, value_t value) {

    struct SLNode* new = newSLN(value);
    struct SLNode* tmp = pSLCL->entry;

    if(new == NULL) {
        errno = ESLLMEMORYFAIL;
        return pSLCL->entry;
    }

    if(pSLCL->entry != NULL) {
        setNextSLN(new, pSLCL->entry);
        while(tmp->next != pSLCL->entry) {
            tmp = tmp->next; //On cherche le dernier élément de la liste
        }                    //Pour que son suivant soit le nouveau (circulaire)
        setNextSLN(tmp, new);
    } else {
        setNextSLN(new, new);
    }

    //return NULL;

    pSLCL->entry = new;
    return pSLCL->entry;
}

struct SLNode* insertSLCL(struct SLCircularList* pSLCL, struct SLNode* pSLN, value_t value) {

    if(pSLCL->entry != NULL && pSLN == NULL
    || pSLCL->entry == NULL && pSLN != NULL) {
        // Si la liste n'est pas vide, on ne peut pas avoir un Node vide
        errno = ESLLMEMORYFAIL;
        return pSLN;
    }
    
    struct SLNode* new = newSLN(value);
    struct SLNode* tmp = pSLCL->entry;
    
    if(new == NULL) {
        // Erreur lors de l'instanciation du nouveau Node
        errno = ESLLMEMORYFAIL;
        return pSLN;
    }

    if(pSLCL->entry == NULL || pSLCL->entry == pSLN) {
        // On veut insérer le nouvel élement en tête de liste
        setNextSLN(new, pSLCL->entry);
        while(tmp->next != pSLCL->entry) {
            tmp = tmp->next; //On cherche le dernier élément de la liste
        }                    //Pour que son suivant soit le nouveau (circulaire)
        setNextSLN(tmp, new);
        pSLCL->entry = new;
        return pSLCL->entry;
    }

    // On doit trouver l'élément précédant pSLN

    struct SLNode* previous = pSLCL->entry;

    while(previous->next != pSLN) {
        previous = previous->next;
    }

    setNextSLN(previous, new);
    setNextSLN(new, pSLN);

    return new;
}

struct SLNode* popSLCL(struct SLCircularList* pSLCL) {

    struct SLNode* last = pSLCL->entry;
    struct SLNode* second = pSLCL->entry->next;

    if(pSLCL->entry == NULL) {
        errno = ESLLEMPTY;
        return NULL;
    }

    while(last->next != pSLCL->entry) { //On cherche le dernier élément de la liste
        last = last->next;              //Pour qu'il pointe vers le second
    }                                   //Car on va supprimer le premier
    setNextSLN(last, second);

    deleteSLN(&(pSLCL->entry));
    pSLCL->entry = second;
}

struct SLNode* eraseSLCL(struct SLCircularList* pSLCL, struct SLNode* pSLN) {

    struct SLNode* last = pSLCL->entry;
    struct SLNode* previous = pSLCL->entry;

    if(pSLCL->entry == NULL) {
        return NULL;
    }

    while(last->next != pSLCL->entry) {
        last = last->next;
    }

    if(last == pSLCL->entry) { //Cas particulier s'il n'y a qu'un seul Node dans la liste
        deleteSLN(&pSLN);
        pSLCL->entry = NULL;
        return NULL;
    }

    while(previous->next != pSLN) {
        previous = previous->next;
    }

    previous->next = pSLN->next;

    deleteSLN(&pSLN);

    return previous->next;
}

int main() {
    printf("Hello world!\n");
    struct SLCircularList* test = newSLCL();
    pushSLCL(test, 7);
    pushSLCL(test, 14);
    printf("size: %lu\n", sizeSLCL(test));
    return 0;
}