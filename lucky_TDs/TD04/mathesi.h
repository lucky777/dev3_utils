#ifndef MATHESI
#define MATHESI
#include <stdbool.h>

struct PrimeFactor {
    unsigned value;
    unsigned multiplicity;
};

struct PrimeFactorization {
    unsigned nb;
    unsigned count;
    struct PrimeFactor* primeFactors;
};

bool isPrime(unsigned);
void printPrimes(unsigned, unsigned);
unsigned nextPrime(unsigned);
void printPrimeFactor(unsigned, bool);
unsigned gcd(unsigned, unsigned);
void printGcd();
unsigned* primeFactorA(unsigned*, unsigned);
unsigned primeFactorB(unsigned**, unsigned**, unsigned);
struct PrimeFactor* primeFactorC(unsigned*, unsigned);
void primeFactorD(struct PrimeFactorization*);

#endif