#include "mathesi.h"
#include <stdlib.h>
#include <stdio.h>

int main() {

    //ex4.1
    printf("========================================\n");
    printf("ex4.1 primeFactorA(&count, 84) :\n");

    unsigned count = 0;
    unsigned* tab = primeFactorA(&count, 84);

    for(int i=0; i<count; i++) {
        printf("-> %u\n", tab[i]);
    }

    free(tab);

    //ex4.2
    printf("========================================\n");
    printf("ex4.2 primeFactorB(factor, mult, 84) :\n");

    unsigned* factor;
    unsigned* multiplicity;
    unsigned count2 = primeFactorB(&factor, &multiplicity, 84);

    printf("Factor: \n");
    for(int i=0; i<count2; i++) {
        printf("-> %u\n", factor[i]);
    }

    printf("Multiplicity: \n");
    for(int i=0; i<count2; i++) {
        printf("-> %u\n", multiplicity[i]);
    }

    free(factor);
    free(multiplicity);

    //ex4.3
    printf("========================================\n");
    printf("ex4.3 primeFactorC(count, 84) :\n");
    unsigned nbElem = 0;
    struct PrimeFactor* pf = primeFactorC(&nbElem, 84);

    for(int i=0; i<nbElem; i++) {
        printf("Struct nb %d:\n\tvalue=%d\n\tmultiplicity=%d\n\n", (i+1), pf[i].value, pf[i].multiplicity);
    }

    free(pf);

    //ex4.4
    printf("========================================\n");
    printf("ex4.4 primeFactorD(&f) :\n");
    struct PrimeFactorization f = {84, 0, NULL};
    primeFactorD(&f);
    //f.nb = 84
    //f.count = 3
    //f.primeFactors = {{2, 2}, {3, 1}, {7, 1}}

    for(int i=0; i<f.count; i++) {
        printf("Struct nb %d:\n\tvalue=%d\n\tmultiplicity=%d\n\n", (i+1), f.primeFactors[i].value, f.primeFactors[i].multiplicity);
    }

    return 0;
}