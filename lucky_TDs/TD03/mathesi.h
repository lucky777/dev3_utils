#ifndef MATHESI
#define MATHESI
#include <stdbool.h>

bool isPrime(unsigned);
void printPrimes(unsigned, unsigned);
unsigned nextPrime(unsigned);
void printPrimeFactor(unsigned, bool);
unsigned gcd(unsigned, unsigned);
void printGcd();

#endif