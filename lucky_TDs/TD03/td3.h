#ifndef TD3
#define TD3

#include <stdbool.h>
#include <stddef.h>

size_t strlen(const char*);
int strcmp(const char*, const char*);
char* strcpy(char*, const char*);
char* strncpy(char*, const char*, size_t);
char* strcat(char*, const char*);
char* strncat(char*, const char*, size_t);
char* strtok(char*, const char*);

#endif